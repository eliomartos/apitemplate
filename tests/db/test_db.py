import json
import mariadb
import pytest as pt
import uuid

from dotenv import load_dotenv

from app.models import app_queries
from tests import app, client, credentials

class Params:
    identifier = str(uuid.uuid4())

class Nspace:
    id = Params().identifier

@pt.fixture()
def connection(credentials):
    credentials.pop("database")
    connection = mariadb.connect(**credentials)
    yield connection
    connection.commit()
    connection.close()

class TestDB(object):

    @staticmethod
    def test_startup():
        pass

    def test_db_connection(self, connection):
        cursor = connection.cursor()
        return 0
    
    def test_db_creation(self, connection):
        cursor = connection.cursor()
        cursor.execute("CREATE DATABASE IF NOT EXISTS brainstem;")
        return 0

    def test_db_bad_input(self, connection):
        with pt.raises(mariadb.ProgrammingError):
            cursor = connection.cursor()
            cursor.execute("CREATE error;")

    def test_db_create_table(self, connection):
        cursor = connection.cursor()
        cursor.execute(app_queries["create_table_user"])
        return 0
    
    def test_db_insert(self, connection):
        payload = [Nspace.id,"admin", "testing@test.com"]
        cursor = connection.cursor()
        cursor.execute(app_queries["create_user"], payload)
        return 0
    
    def test_db_hit(self, connection):
        payload = [Nspace.id]
        cursor = connection.cursor()
        cursor.execute(app_queries["read_user"], payload)
        results = cursor.fetchall()
        assert type(results)==list
        assert len(results)==1
        assert type(results[0])==tuple
        assert type(results[0][0])==str
        assert type(results[0][1])==str
        assert type(results[0][2])==str
        return 0
    
    def test_db_update(self, connection):
        payload = ["new", "new@new.com", Nspace.id]
        cursor = connection.cursor()
        cursor.execute(app_queries["update_user"], payload)
        connection.commit()
        cursor.execute(app_queries["read_user"], [Nspace.id])
        results = cursor.fetchall()
        assert results[0][1]=="new"
        assert results[0][2]=="new@new.com"
        return 0
    
    def test_db_delete(self, connection):
        payload = [Nspace.id]
        cursor = connection.cursor()
        cursor.execute(app_queries["delete_user"], payload)
        connection.commit()
        cursor.execute(app_queries["read_user"], payload)
        results = cursor.fetchall()
        assert len(results)==0
        return 0

    @staticmethod
    def test_teardown(connection):
        cursor = connection.cursor()
        cursor.execute("DROP DATABASE brainstem;")
        return 0
        