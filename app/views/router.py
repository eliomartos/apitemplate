import flask


views = flask.Blueprint("views", __name__)


@views.route("/")
def landing():
    return flask.render_template("landing.html")
