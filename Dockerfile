FROM ubuntu
MAINTAINER Trijeet Sethi <trijeet@protonmail.com>

RUN apt-get update --fix-missing
RUN apt-get install python-dev python3-pip libmariadb3 --assume-yes
RUN apt-get update --fix-missing 
RUN apt-get install libmariadb-dev mariadb-server --assume-yes

COPY . /apitemplate
WORKDIR /apitemplate

RUN python3 -m pip install -r ./app/requirements.txt

EXPOSE 3000

RUN python3 manager.py --seed
ENTRYPOINT [ "python3", "manager.py" ]